---
layout: markdown_page
title: "Internship Working Group"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property        | Value           |
|-----------------|-----------------|
| Date Created    | October 2, 2019 |
| Target End Date | TBD             |
| Slack           | [#wg_internship](https://gitlab.slack.com/archives/CNLLV1NEN/) (only accessible from within the company) |
| Google Doc      | [Internship Working Group Agenda](https://docs.google.com/document/d/1KsdRtkRcF4EOpL2s0JDC-th8SB7nHstw8cfceLrzG-o/edit#) (only accessible from within the company) |
| Issue Board     | TBD             |

## Business Goal

TBD

## Exit Criteria (30%)

TBD

## Roles and Responsibilities

| Working Group Role    | Person                | Title                          |
|-----------------------|-----------------------|--------------------------------|
| Executive Sponsor     | Eric Johnson          | VP of Engineering              |
| Facilitator           | Jean du Plessis       | Frontend Engineering Manager   |
| Functional Lead       | Roos Takken           | People Business Partner, Engineering |
| Functional Lead       | Nick Nguyen           | Engineering Manager, Ecosystem |
| Member                | Tanya Pazitny         | Quality Engineering Manager    |
| Member                | Clement Ho            | Frontend Engineering Manager   |
| Member                | Phil Calder           | Growth Engineering Manager     |
| Member                | Liam McNally          | Interim Recruting Manager      |
| Member                | John Hope             | Engineering Manager, Plan      |

